using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace jotron {
    class UnitTestRecordFinder {
        
        [TestMethod]
        public void TestFilter()
        {
            RecordFactory factory = new RecordFactory();
            Record r1 = factory.GetRecord("1,2,3,a");
            
            RecordFinder finder = new RecordFinder();

            finder.SetFilter("*","*");
            Assert.IsTrue(finder.filter(r1));
            finder.SetFilter("*","3");
            Assert.IsTrue(finder.filter(r1));
            finder.SetFilter("2","3");
            Assert.IsTrue(finder.filter(r1));
            finder.SetFilter("1","3");
            Assert.IsFalse(finder.filter(r1));
            finder.SetFilter("2","*");
            Assert.IsTrue(finder.filter(r1));
        }
    }
}