
using System;
using System.IO;

namespace jotron {
class Record : IComparable<Record>
    {
        public int Id;
        public byte Type;
        public ushort SubType;
        public string Data;

        public Record()
        {
            
        }

        public string ToCsv()
        {
            return this.Id+","+this.Type+","+this.SubType+","+this.Data;
        }

        public override string ToString()
        {
            return "ID: "+this.Id+" Type="+this.Type+" SubType="+this.SubType+" Data="+this.Data;
        }

        // Useful for sorting Records. 
        // 1st priority Type
        // 2nd priority Subtype
        // 3rd priority Id
        public int CompareTo(Record r)
        {
            if (r == null) return 1;
            int comparedType = this.Type.CompareTo(r.Type);
            if (comparedType == 0)
            {
                int comparedSubtype = this.SubType.CompareTo(r.SubType);
                if (comparedSubtype == 0)
                {
                    return this.Id.CompareTo(r.Id);
                } else {
                    return comparedSubtype;
                }
            } else {
                return comparedType;
            }
        }
    }
}