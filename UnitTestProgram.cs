using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace jotron
{
    [TestClass]
    public class UnitTestProgram
    {
        

        [TestMethod]
        public void TestParser()
        {
            string[] args = {"-g", "--subtype=22", "--type=11", "myfile"};
            (string filename, bool doGenerate, bool doSearch, string type, string subtype) = Program.ParseArguments(args);
            Assert.AreEqual(filename, "myfile");
            Assert.IsTrue(doGenerate);
            Assert.AreEqual(subtype, "22");
            Assert.AreEqual(type, "11");
            Assert.IsFalse(doSearch);
        }
    }
}
