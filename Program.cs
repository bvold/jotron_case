﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace jotron
{
    class Program
    {
         static void Main(string[] args)
        {
            (string filename, bool doGenerate, bool doSearch, string type, string subtype) = ParseArguments(args);
            
            if (!doGenerate && !doSearch)
            {
                Console.WriteLine("Must specify either --generate or --search");
                PrintHelp();
            }

            if (doGenerate) GenerateRecordFile(filename, new Random().Next(100000,200000));

            if (doSearch)
            {
                RecordFinder finder = new RecordFinder();
                finder.SetFilter(type, subtype);
                List<Record> records = finder.FindRecords(filename);
                records.Sort();
                foreach (Record r in records) Console.WriteLine(r);
            }
        }

        static public Tuple<string, bool, bool, string, string> ParseArguments(string[] args)
        {
            if (args.Length == 0) PrintHelp();
            string filename = args[args.Length-1];
            bool doGenerate = false;
            bool doSearch = false;
            string type = "*";
            string subtype = "*";

            for(int i=0;i<args.Length-1;i++) {
                if (args[i].StartsWith("--subtype=")) {
                    subtype = args[i].Split("=")[1];
                } else if (args[i].StartsWith("--type=")) {
                    type = args[i].Split("=")[1];
                } else if (args[i] == "--generate" || args[i] == "-g") {
                    doGenerate = true;
                } else if (args[i] == "--search" || args[i] == "-s") {
                    doSearch = true;
                } else if (args[i] == "--help" || args[i] == "-h") {
                    PrintHelp();
                }
            }
            return new Tuple<string, bool, bool, string, string>(filename, doGenerate, doSearch, type, subtype);
        }

        static void PrintHelp()
        {
            Console.WriteLine("Usage:");
            Console.WriteLine("Either --generate or --search is required");
            Console.WriteLine("To generate file use --generate or -g as argument. Filename is the last argument. All other arguments will be ignored.");
            Console.WriteLine("Example: ./jotron -g records.csv");
            Console.WriteLine("To search from a file use --search or -s as argument. subtype and/or type can be specified using --subtype=mysubtype or --type=mytype");
            Console.WriteLine("Searching without specifying type or subtype prints all records from the file");
            Console.WriteLine("Example: ./jotron -s subtype=12 type=10 records.csv");
            Environment.Exit(1);
        }

        static void GenerateRecordFile(string filename, int numberOfRecords)
        {
            using (StreamWriter file = new StreamWriter(filename))
            {
                RecordFactory factory = new RecordFactory();
                for(int i=0;i<numberOfRecords;i++) {
                    file.WriteLine(factory.GetRecord().ToCsv());
                }
            }
        }
    }
}
