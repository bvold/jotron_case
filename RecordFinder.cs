using System;
using System.Collections.Generic;
using System.IO;

namespace jotron {
    class RecordFinder {
        public Predicate<Record> filter = (x => true);

        public RecordFinder()
        {
        }

        public void SetFilter(string type, string subtype)
        {
            if (type == "*" && subtype != "*")
            {
                ushort subtypeVal = RecordFactory.ValidateSubtype(subtype);
                this.filter = (x => x.SubType == subtypeVal);
            } else if (type != "*" && subtype == "*")
            {
                byte typeVal = RecordFactory.ValidateType(type);
                this.filter = (x => x.Type == typeVal);
            } else if (type != "*" && type != "*")
            {
                byte typeVal = RecordFactory.ValidateType(type);
                ushort subtypeVal = RecordFactory.ValidateSubtype(subtype);
                this.filter = (x => x.SubType == subtypeVal && x.Type == typeVal);
            }
        }

        public List<Record> FindRecords(string filename)
        {
            StreamReader reader = new StreamReader(filename);
            string line;
            RecordFactory recordFactory = new RecordFactory();
            List<Record> records = new List<Record>();
            while ((line = reader.ReadLine()) != null)
            {
                Record r = recordFactory.GetRecord(line);
                if (filter(r)) records.Add(r);
            }
            return records;
        }

    }
}
