using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace jotron
{
    [TestClass]
    public class UnitTestRecord
    {
        List<Record> records = new List<Record>();
        RecordFactory factory;
        public UnitTestRecord()
        {
            factory = new RecordFactory();
            for(int i=0;i<1000;i++) records.Add(factory.GetRecord());
        }
        
        [TestMethod]
        public void TestProperties()
        {
            List<Record> recordsUnique = records.GroupBy(x => x.Id).Select(x => x.First()).ToList();
            Assert.AreEqual(recordsUnique.Count, records.Count);
            
            foreach (Record r in records)
            {
                Assert.IsTrue(r.Type >= RecordFactory.minType);
                Assert.IsTrue(r.Type <= RecordFactory.maxType);
                Assert.IsTrue(r.SubType >= RecordFactory.minSubtype);
                Assert.IsTrue(r.SubType <= RecordFactory.maxSubtype);
                Assert.IsNotNull(r.Data);
            }
        }

        [TestMethod]
        public void TestComparator()
        {
            Record r1 = factory.GetRecord("1,2,3,a");
            Record r2 = factory.GetRecord("8,3,4,a");
            Record r3 = factory.GetRecord("2,2,3,a");
            Record r4 = factory.GetRecord("7,3,3,a");
            Record r5 = factory.GetRecord("9,1,3,a");

            Assert.AreEqual(r1.ToCsv(), "1,2,3,a");

            List<Record> rs = new List<Record>(){r1,r2,r3,r4,r5};
            rs.Sort();
            Assert.AreEqual(rs[0].Id, 9);
            Assert.AreEqual(rs[1].Id, 1);
            Assert.AreEqual(rs[2].Id, 2);
            Assert.AreEqual(rs[3].Id, 7);
            Assert.AreEqual(rs[4].Id, 8);
        }

        [TestMethod]
        public void TestConstructor()
        {
            Record r = factory.GetRecord("9,1,3,a");

            Assert.AreEqual(r.Id, 9);
            Assert.AreEqual(r.Type, 1);
            Assert.AreEqual(r.SubType, 3);
            Assert.AreEqual(r.Data, "a");

            try {
                factory.GetRecord("1,2,3,4,5");
                Assert.Fail();
            } catch(FormatException) {}

            try {
                factory.GetRecord("1,a,3,4");
                Assert.Fail();
            } catch(FormatException) {}

            try {
                factory.GetRecord("1,1,0,5");
                Assert.Fail();
            } catch(InvalidDataException) {}

            try {
                factory.GetRecord("1,0,1,5");
                Assert.Fail();
            } catch(InvalidDataException) {}
        }
    }
}
