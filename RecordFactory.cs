using System;
using System.IO;

namespace jotron {
    class RecordFactory {

        private static int nextId = 0;
        public static ushort minSubtype = 1;
        public static ushort maxSubtype = 300;
        public static byte minType = 1;
        public static byte maxType = 99;

        private Random r;

        public RecordFactory()
        {
            r = new Random();
        }

        public Record GetRecord(string csvLine)
        {
            Record record = new Record();
            string[] attributes = csvLine.Split(",");
            if (attributes.Length != 4) throw new FormatException("Illegal record string");
            record.Id = Int32.Parse(attributes[0]);
            record.Type = ValidateType(attributes[1]);
            record.SubType = ValidateSubtype(attributes[2]);
            record.Data = attributes[3];
            return record;
        }

        public Record GetRecord()
        {
            Record record = new Record();
            AssignId(record);
            
            record.Type = (byte)r.Next(1,100);
            record.SubType = (ushort)r.Next(1,301);
            record.Data = GetRandomString();
            return record;
        }

        void AssignId(Record record)
        {
            record.Id = nextId;
            nextId++;                                                                  //This might overflow, but numbers are still unique until 1 is reached the second time
            if (nextId == 0) throw new OverflowException("Unique ID overflow");        //We have created UInt32.MaxValue unique IDs. We could have created one more, but the number overflows here.
        }

        static string GetRandomString()
        {
            Random r = new Random();
            string selection = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
            int stringLength = r.Next(1,128);
            string randomString = "";
            for (int i=0;i<stringLength;i++)
            {
                randomString += selection[r.Next(0,selection.Length-1)];
            }
            return randomString;
        }

        static public ushort ValidateSubtype(string st)
        {
            ushort subtypeVal = ushort.Parse(st);
            if (subtypeVal < minSubtype || subtypeVal > maxSubtype) throw new InvalidDataException("Illegal subtype specified. subtype="+st);
            return subtypeVal;
        }

        static public byte ValidateType(string t)
        {
            byte typeVal = byte.Parse(t);
            if (typeVal < minType || typeVal > maxType) throw new InvalidDataException("Illegal type specified. type="+t);
            return typeVal;
        }
    }
}